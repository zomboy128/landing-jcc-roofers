<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class LandingController extends Controller
{
    public function index()
    {
        return view('landing.index');
    } 

    public function store(Request $request)
    {
            $name = $request->name;
            $phone = $request->phone;
            $service = $request->service;
            $subject = "Houston's Trusted Roofers";
            $for = $request->email;
            Mail::send('email.contact',compact('name','phone','service','for'),function($msj) use ($subject,$for)
            {
                $msj->from("no-reply@myroofimprovement.com","Houston's Trusted Roofers");
                $msj->subject($subject);
                $msj->to("jennifer@jccroofing.com");
            });
    }
}

