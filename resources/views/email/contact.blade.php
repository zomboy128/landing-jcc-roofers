<!--==============Bienvenida Usuarios================-->
<div class="es-wrapper-color prueba"
style="background: #26718F url(https://www.myroofimprovement.com/assets/img/work-bg/bg-2.jpg) !important;">
    <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td class="esd-email-paddings" valign="top" style="padding: 50px;">
                    <table class="esd-header-popover es-header" cellspacing="0" cellpadding="0" align="center">
                        <tbody>
                            <tr>
                                <td class="esd-stripe" align="center" bgcolor="transparent"
                                    style="background-color: transparent;" esd-custom-block-id="78484">
                                    <table class="es-header-body" width="600" cellspacing="0" cellpadding="0"
                                        bgcolor="transparent" align="center" style="background-color: transparent;">
                                        <tbody>
                                            <tr>
                                                <td class="esd-structure es-p30t es-p30b es-p20r es-p20l" align="left"
                                                    bgcolor="#ffffff" style="background-color: #ffffff;">
                                                    <table class="es-left" cellspacing="0" cellpadding="0" align="left">
                                                        <tbody>
                                                            <tr>
                                                                <td class="es-m-p0r esd-container-frame es-m-p20b"
                                                                    width="500" valign="top" align="center">
                                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="center"
                                                                                    style="padding: 20px !important;"
                                                                                    class="esd-block-text">
                                                                                    <h1
                                                                                        style="font-size: 29px;margin: 0px;">
                                                                                        <span
                                                                                            style="color:#FFFFFF;"><span
                                                                                                style="background-color:#1F2D3D ;border-radius: 3px;padding: 0px 5px;">Houston's</span></span>
                                                                                                Trusted Roofers
                                                                                    </h1>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table cellspacing="0" cellpadding="0" align="right">
                                                        <tbody>
                                                            <tr>
                                                                <td class="esd-container-frame" width="360"
                                                                    align="left">
                                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="esd-block-menu"
                                                                                    esd-tmp-menu-font-weight="bold"
                                                                                    esd-tmp-menu-color="#273444"
                                                                                    esd-tmp-menu-font-style="normal">
                                                                                    <table cellpadding="0"
                                                                                        cellspacing="0" width="30%"
                                                                                        class="es-menu">
                                                                                        <tbody>
                                                                                            <tr class="links">
                                                                                                <td valign="top">
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table cellpadding="0" cellspacing="0" class="es-content" align="center">
                        <tbody>
                            <tr>
                                <td class="esd-stripe" align="center" bgcolor="transparent"
                                    style="background-color: transparent;">
                                    <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0"
                                        cellspacing="0" width="600" style="background-color: #ffffff;">
                                        <tbody>
                                            <tr>
                                                <td class="esd-structure" align="left" bgcolor="transparent"
                                                    style="background-color: transparent; background-position: left top;"
                                                    esd-custom-block-id="78485">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                            <tr>
                                                                <td width="600" class="esd-container-frame"
                                                                    align="center" valign="top">
                                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                                        bgcolor="#333333"
                                                                        style="background-color: #1F2D3D ; border-radius: 0px; border-collapse: separate;padding: 5px;padding-top: 0px;padding-bottom: 25px;">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="center"
                                                                                    class="esd-block-text">
                                                                                    <h1
                                                                                        style="color:#000;margin-top: 40px;margin-bottom: 0px; font-size: 38px; line-height: 120%;text-align: center;">
                                                                                        <strong><span
                                                                                                style="background-color: #ffffff;border-radius: 3px;">&nbsp;WELCOME&nbsp;</span></strong>
                                                                                    </h1>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center"
                                                                                    class="esd-block-text">
                                                                                    <p
                                                                                        style="color: #ffffff; font-size: 11px; line-height: 200%; text-align: center; margin-top: 10px;">
                                                                                        <strong><em>Regards,
                                                                                                {{ $name }} wishes to
                                                                                                contact Houston's
                                                                                                Trusted
                                                                                                Roofers.</em></strong>
                                                                                    </p>
                                                                                    <p
                                                                                        style="color: #ffffff; font-size: 11px; line-height: 200%; text-align: center; margin-top: 10px;">
                                                                                        <strong><em>then we will detail
                                                                                                the quote:</em></strong>
                                                                                    </p>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center"
                                                                                    class="esd-block-spacer es-p20"
                                                                                    style="font-size:0">
                                                                                    <table border="0" width="100%"
                                                                                        height="100%" cellpadding="0"
                                                                                        cellspacing="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td
                                                                                                    style="border-bottom: 0px solid #cccccc; background: none; height: 1px; width: 100%; margin: 0px;">
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <br>
                                            <tr>
                                                <td class="esd-structure es-p40t es-p20b es-p20r es-p20l" align="left"
                                                    esd-custom-block-id="78492">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                        style="margin-top: 10px;">
                                                        <tbody>
                                                            <tr>
                                                                <td width="560" class="esd-container-frame"
                                                                    align="center" valign="top">
                                                                    <table style="width:100%">
                                                                        <tr>
                                                                            <th><strong
                                                                                    style="font-size:11px">Phone</strong>
                                                                            </th>
                                                                            <th><strong
                                                                                    style="font-size:11px">Email</strong>
                                                                            </th>
                                                                            <th><strong
                                                                                    style="font-size:11px">service</strong>
                                                                            </th>
                                                                        </tr>
                                                                        <tr>
                                                                            <td
                                                                                style="width: 33.33%; text-align: center;font-size: 11px;background: #F0F0F0;padding: 5px; color: #666666;font-weight: bold;">
                                                                                {{ $phone }}</td>
                                                                            <td
                                                                                style="width: 33.33%; text-align: center;font-size: 11px;background: #F0F0F0;padding: 5px; color: #666666;font-weight: bold;">
                                                                                {{ $for }}</td>
                                                                            <td
                                                                                style="width: 33.33%; text-align: center;font-size: 11px;background: #F0F0F0;padding: 5px; color: #666666;font-weight: bold;">
                                                                                {{ $service }}</td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                          </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <br><br>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<style>
#outlook a {
    padding: 0;
}

.ExternalClass {
    width: 100%;
}

.ExternalClass,
.ExternalClass p,
.ExternalClass span,
.ExternalClass font,
.ExternalClass td,
.ExternalClass div {
    line-height: 100%;
}

.es-button {
    mso-style-priority: 100 !important;
    text-decoration: none !important;
}

a[x-apple-data-detectors] {
    color: inherit !important;
    text-decoration: none !important;
    font-size: inherit !important;
    font-family: inherit !important;
    font-weight: inherit !important;
    line-height: inherit !important;
}

.es-desk-hidden {
    display: none;
    float: left;
    overflow: hidden;
    width: 0;
    max-height: 0;
    line-height: 0;
    mso-hide: all;
}

.es-button-border:hover a.es-button {
    background: #f32043 !important;
    border-color: #f32043 !important;
}

.es-button-border:hover {
    background: transparent !important;
}

/*
END OF IMPORTANT
*/
s {
    text-decoration: line-through;
}

html,
body {
    width: 100%;
    font-family: helvetica, 'helvetica neue', arial, verdana, sans-serif;
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
}

table {
    mso-table-lspace: 0pt;
    mso-table-rspace: 0pt;
    border-collapse: collapse;
    border-spacing: 0px;
}

table td,
html,
body,
.es-wrapper {
    padding: 0;
    Margin: 0;
}

.es-content,
.es-header,
.es-footer {
    table-layout: fixed !important;
    width: 100%;
}

img {
    display: block;
    border: 0;
    outline: none;
    text-decoration: none;
    -ms-interpolation-mode: bicubic;
}

table tr {
    border-collapse: collapse;
}

p,
hr {
    Margin: 0;
}

h1,
h2,
h3,
h4,
h5 {
    Margin: 0;
    line-height: 120%;
    mso-line-height-rule: exactly;
    font-family: helvetica, 'helvetica neue', arial, verdana, sans-serif;
}

p,
ul li,
ol li,
a {
    -webkit-text-size-adjust: none;
    -ms-text-size-adjust: none;
    mso-line-height-rule: exactly;
}

.es-left {
    float: left;
}

.es-right {
    float: right;
}

.es-p5 {
    padding: 5px;
}

.es-p5t {
    padding-top: 5px;
}

.es-p5b {
    padding-bottom: 5px;
}

.es-p5l {
    padding-left: 5px;
}

.es-p5r {
    padding-right: 5px;
}

.es-p10 {
    padding: 10px;
}

.es-p10t {
    padding-top: 10px;
}

.es-p10b {
    padding-bottom: 10px;
}

.es-p10l {
    padding-left: 10px;
}

.es-p10r {
    padding-right: 10px;
}

.es-p15 {
    padding: 15px;
}

.es-p15t {
    padding-top: 15px;
}

.es-p15b {
    padding-bottom: 15px;
}

.es-p15l {
    padding-left: 15px;
}

.es-p15r {
    padding-right: 15px;
}

.es-p20 {
    padding: 20px;
}

.es-p20t {
    padding-top: 20px;
}

.es-p20b {
    padding-bottom: 20px;
}

.es-p20l {
    padding-left: 20px;
}

.es-p20r {
    padding-right: 20px;
}

.es-p25 {
    padding: 25px;
}

.es-p25t {
    padding-top: 25px;
}

.es-p25b {
    padding-bottom: 25px;
}

.es-p25l {
    padding-left: 25px;
}

.es-p25r {
    padding-right: 25px;
}

.es-p30 {
    padding: 30px;
}

.es-p30t {
    padding-top: 20px;
}

.es-p30b {
    padding-bottom: 20px;
}

.es-p30l {
    padding-left: 30px;
}

.es-p30r {
    padding-right: 30px;
}

.es-p35 {
    padding: 35px;
}

.es-p35t {
    padding-top: 35px;
}

.es-p35b {
    padding-bottom: 35px;
}

.es-p35l {
    padding-left: 35px;
}

.es-p35r {
    padding-right: 35px;
}

.es-p40 {
    padding: 40px;
}

.es-p40t {
    padding-top: 20px;
}

.es-p40b {
    padding-bottom: 40px;
}

.es-p40l {
    padding-left: 40px;
}

.es-p40r {
    padding-right: 40px;
}

.es-menu td {
    border: 0;
}

.es-menu td a img {
    display: inline-block !important;
}

/* END CONFIG STYLES */
a {
    font-family: helvetica, 'helvetica neue', arial, verdana, sans-serif;
    font-size: 14px;
    text-decoration: underline;
}

h1 {
    font-size: 25px;
    font-style: normal;
    font-weight: bold;
    color: #333333;
}

h1 a {
    font-size: 30px;
}

h2 {
    font-size: 24px;
    font-style: normal;
    font-weight: bold;
    color: #333333;
}

h2 a {
    font-size: 24px;
}

h3 {
    font-size: 18px;
    font-style: normal;
    font-weight: bold;
    color: #333333;
}

h3 a {
    font-size: 18px;
}

p,
ul li,
ol li {
    font-size: 14px;
    font-family: helvetica, 'helvetica neue', arial, verdana, sans-serif;
    line-height: 150%;
}

ul li,
ol li {
    Margin-bottom: 15px;
}

.es-menu td a {
    text-decoration: none;
    display: block;
}

.es-wrapper {
    width: 100%;
    height: 100%;
    background-repeat: repeat;
    background-position: center top;
}

body {
    color: #666666 !important;
}

.es-content-body a {
    color: #d60b2c;
}

.es-header {
    background-color: transparent;
    background-repeat: repeat;
    background-position: center top;
}

.es-header-body {
    background-color: transparent;
}

.es-header-body p,
.es-header-body ul li,
.es-header-body ol li {
    color: #8492a6;
    font-size: 12px;
}

.es-header-body a {
    color: #ffffff;
    font-size: 12px;
}

.es-footer {
    background-color: transparent;
    background-repeat: repeat;
    background-position: center top;
}

.es-footer-body {
    background-color: transparent;
}

.es-footer-body p,
.es-footer-body ul li,
.es-footer-body ol li {
    color: #ffffff;
    font-size: 10px;
}

.es-footer-body a {
    color: #ffffff;
    font-size: 12px;
}

.es-infoblock,
.es-infoblock p,
.es-infoblock ul li,
.es-infoblock ol li {
    line-height: 120%;
    font-size: 12px;
    color: #cccccc;
}

.es-infoblock a {
    font-size: 12px;
    color: #cccccc;
}

a.es-button {
    border-style: solid;
    border-color: #d60b2c;
    border-width: 10px 25px 10px 25px;
    display: inline-block;
    background: #d60b2c;
    border-radius: 4px;
    font-size: 16px;
    font-family: helvetica, 'helvetica neue', arial, verdana, sans-serif;
    font-weight: normal;
    font-style: normal;
    line-height: 120%;
    color: #ffffff;
    text-decoration: none;
    width: auto;
    text-align: center;
}

.es-button-border {
    border-style: solid solid solid solid;
    border-color: transparent transparent transparent transparent;
    background: #d60b2c;
    border-width: 0px 0px 0px 0px;
    display: inline-block;
    border-radius: 4px;
    width: auto;
}

.msohide {
    mso-hide: all;
}

/* RESPONSIVE STYLES Please do not delete and edit CSS styles below. If you don't need responsive layout, please delete this section. */
@media only screen and (max-width: 600px) {

    p,
    ul li,
    ol li,
    a {
        font-size: 14px !important;
        line-height: 150% !important;
    }

    h1 {
        font-size: 30px !important;
        text-align: center;
        line-height: 120%;
    }

    h2 {
        font-size: 26px !important;
        text-align: center;
        line-height: 120%;
    }

    h3 {
        font-size: 20px !important;
        text-align: center;
        line-height: 120%;
    }

    h1 a {
        font-size: 30px !important;
    }

    h2 a {
        font-size: 26px !important;
    }

    h3 a {
        font-size: 20px !important;
        text-align: center;
    }

    .es-menu td a {
        font-size: 12px !important;
    }

    .es-header-body p,
    .es-header-body ul li,
    .es-header-body ol li,
    .es-header-body a {
        font-size: 12px !important;
    }

    .es-footer-body p,
    .es-footer-body ul li,
    .es-footer-body ol li,
    .es-footer-body a {
        font-size: 12px !important;
    }

    .es-infoblock p,
    .es-infoblock ul li,
    .es-infoblock ol li,
    .es-infoblock a {
        font-size: 12px !important;
    }

    *[class="gmail-fix"] {
        display: none !important;
    }

    .es-m-txt-c,
    .es-m-txt-c h1,
    .es-m-txt-c h2,
    .es-m-txt-c h3 {
        text-align: center !important;
    }

    .es-m-txt-r,
    .es-m-txt-r h1,
    .es-m-txt-r h2,
    .es-m-txt-r h3 {
        text-align: right !important;
    }

    .es-m-txt-l,
    .es-m-txt-l h1,
    .es-m-txt-l h2,
    .es-m-txt-l h3 {
        text-align: center !important;
    }

    .es-m-txt-r img,
    .es-m-txt-c img,
    .es-m-txt-l img {
        display: inline !important;
    }

    .es-button-border {
        display: inline-block !important;
    }

    a.es-button {
        font-size: 16px !important;
        display: inline-block !important;
    }

    .es-btn-fw {
        border-width: 10px 0px !important;
        text-align: center !important;
    }

    .es-adaptive table,
    .es-btn-fw,
    .es-btn-fw-brdr,
    .es-left,
    .es-right {
        width: 100% !important;
    }

    .es-content table,
    .es-header table,
    .es-footer table,
    .es-content,
    .es-footer,
    .es-header {
        width: 100% !important;
        max-width: 600px !important;
    }

    .es-adapt-td {
        display: block !important;
        width: 100% !important;
    }

    .adapt-img {
        width: 100% !important;
        height: auto !important;
    }

    .es-m-p0 {
        padding: 0px !important;
    }

    .es-m-p0r {
        padding-right: 0px !important;
    }

    .es-m-p0l {
        padding-left: 0px !important;
    }

    .es-m-p0t {
        padding-top: 0px !important;
    }

    .es-m-p0b {
        padding-bottom: 0 !important;
    }

    .es-m-p20b {
        padding-bottom: 20px !important;
    }

    .es-mobile-hidden,
    .es-hidden {
        display: none !important;
    }

    tr.es-desk-hidden,
    td.es-desk-hidden,
    table.es-desk-hidden {
        width: auto !important;
        overflow: visible !important;
        float: none !important;
        max-height: inherit !important;
        line-height: inherit !important;
    }

    tr.es-desk-hidden {
        display: table-row !important;
    }

    table.es-desk-hidden {
        display: table !important;
    }

    td.es-desk-menu-hidden {
        display: table-cell !important;
    }

    table.es-table-not-adapt,
    .esd-block-html table {
        width: auto !important;
    }

    table.es-social {
        display: inline-block !important;
    }

    table.es-social td {
        display: inline-block !important;
    }

    .g-card td {
        width: 100% !important;
        display: block;
        max-width: 100% !important;
    }

    .g-card amp-img,
    .g-card img {
        border-radius: 8px 8px 0px 0px !important;
    }
}

/* END RESPONSIVE STYLES */
/* CUSTOM STYLES */
.amp-carousel-button {
    background-color: #0c66ff;
    border-radius: 30px;
}

.card amp-img,
.card img {
    border-radius: 8px 8px 0 0;
}

.g-card amp-img,
.g-card img {
    border-radius: 8px 0 0 8px;
}

.section-title {
    padding: 10px 15px;
    background-color: #f6f6f6;
    border: 1px solid #dfdfdf;
    outline: 0;
    border-radius: 8px;
    font-weight: bold !important;
}

/* END CUSTOM STYLES */
.es-p-default {
    padding-top: 20px;
    padding-right: 30px;
    padding-bottom: 0px;
    padding-left: 30px;
}

.es-p-all-default {
    padding: 0px;
}
</style>