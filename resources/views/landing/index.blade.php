@extends('app')
@section('content')
<!--============Navbar==============-->
<div class="top-bar navbar navbar-main navbar-expand-lg d-none d-sm-block">
    <ul class="navbar-nav ml-lg-auto">
        <li class="nav-item nav-item-spaced">
            <a class="nav-link" href="tel:281-498-7663">
                <i data-feather="phone" class="mr-2"></i> 281-498-7663
            </a>
        </li>
        <li class="nav-item nav-item-spaced">
            <a class="nav-link" href="mailto:info@jccroofing.com">
                info@jccroofing.com
            </a>
        </li>
        <li class="nav-item nav-item-spaced">
            <a class="nav-link" href="https://www.myroofimprovement.com/">
                Mon – Fri 8AM – 6PM
            </a>
        </li>
    </ul>
</div>
<!--============Navbar Mobile==============-->
<div class="top-bar navbar navbar-main navbar-expand-lg d-sm-none">
    <div class="row">
        <div class="col-8">
            <p class="font-weight-800 mobile-top-copy">Your new roof as low as $99/month.</p>
        </div>
        <div class="col-4 text-center">
            <a href="tel:281-498-7663" class="btn btn-jcc-blue btn-icon shadow-sm hover-translate-y-n3 mobile-top-cta">
                <span class="btn-inner--text">Call us</span>
            </a>
        </div>
    </div>
</div>

<!--============Header==============-->
<header class="header-transparent pt-3" id="header-main">
    <nav class="navbar navbar-main navbar-expand-xl navbar-light bg-light" id="navbar-main">
        <div class="container-fluid">
            <a class="navbar-brand" href="https://www.myroofimprovement.com/">
                <img alt="Image placeholder" src="{{asset('img/logo.png')}}" id="navbar-logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-main-collapse"
                aria-controls="navbar-main-collapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse navbar-collapse-overlay" id="navbar-main-collapse">
                <div class="position-relative">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbar-main-collapse" aria-controls="navbar-main-collapse" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <i data-feather="x"></i>
                    </button>
                </div>
                <ul class="navbar-nav ml-lg-auto">
                    <li class="nav-item nav-item-spaced">
                        <a class="nav-link" href="https://www.myroofimprovement.com/">
                            Home
                        </a>
                    </li>
                    <li class="nav-item nav-item-spaced dropdown dropdown-animate" data-toggle="hover">
                        <a class="nav-link" data-toggle="dropdown" href="roofing" aria-haspopup="true"
                            aria-expanded="false">
                            Roof Replacement
                        </a>
                        <!--=======Expande=========-->
                        <div class="dropdown-menu dropdown-menu-xl p-0">
                            <div class="row no-gutters">
                                <div class="col-12 col-lg-6">
                                    <div class="dropdown-body dropdown-body-right bg-dropdown-secondary h-100">
                                        <h6 class="dropdown-header">
                                            Roofing solutions
                                        </h6>
                                        <div class="list-group list-group-flush">
                                            <div class="list-group-item bg-transparent border-0 px-0 py-2">
                                                <div class="media d-flex">
                                                    <span class="h6">
                                                        <i data-feather="layout"></i>
                                                    </span>
                                                    <div class="media-body ml-2">
                                                        <a href="https://www.myroofimprovement.com/roofing" class="d-block heading h6 mb-0">Roof
                                                            Replacement</a>
                                                        <small class="text-sm text-muted mb-0">The Best Roof
                                                            Installations!</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="list-group-item bg-transparent border-0 px-0 py-2">
                                                <div class="media d-flex">
                                                    <span class="h6">
                                                        <i data-feather="layout"></i>
                                                    </span>
                                                    <div class="media-body ml-2">
                                                        <a href="https://www.myroofimprovement.com/quickquotes" class="d-block heading h6 mb-0">Quick
                                                            quotes</a>
                                                        <small class="text-sm text-muted mb-0">Ask your quote
                                                            today!</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="list-group-item bg-transparent border-0 px-0 py-2">
                                                <div class="media d-flex">
                                                    <span class="h6">
                                                        <i data-feather="globe"></i>
                                                    </span>
                                                    <div class="media-body ml-2">
                                                        <a href="https://www.myroofimprovement.com/hand-nail" class="d-block h6 mb-0">Hand-Nailed
                                                            Roofs</a>
                                                        <small class="text-sm text-muted mb-0">Why we are the
                                                            best</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6 bg-cover bg-size--cover"
                                    style="background-image: url({{asset('img/house-menu.jpg')}});">

                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item nav-item-spaced">
                        <a class="nav-link" href="https://www.myroofimprovement.com/roof-repair">
                            Roof Repairs
                        </a>
                    </li>
                    <li class="nav-item nav-item-spaced">
                        <a class="nav-link" href="https://www.myroofimprovement.com/roofing-showcase">
                            Our Work
                        </a>
                    </li>
                    <li class="nav-item nav-item-spaced">
                        <a class="nav-link" href="https://www.myroofimprovement.com/roof-insurance">
                            Roof Insurance
                        </a>
                    </li>
                    <li class="nav-item nav-item-spaced">
                        <a class="nav-link" href="https://www.myroofimprovement.com/financing">
                            Roof Financing
                        </a>
                    </li>

                    <li class="nav-item nav-item-spaced">
                        <a class="nav-link" href="https://www.myroofimprovement.com/roofing-insights-blog">
                            Blog
                        </a>
                    </li>

                    <li class="nav-item nav-item-spaced">
                        <a class="nav-link" href="https://www.myroofimprovement.com/contact-jccroofing-company-houston">
                            Contact
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav align-items-lg-center d-none d-lg-flex ml-lg-auto">
                    <li class="nav-item">
                        <a href="https://book.housecallpro.com/book/JCC-Roofing-Company/90f4d54160e34e2daaae20d26db6ad2d"
                            class="btn btn-sm btn-jcc-blue btn-icon ml-3" target="_blank">
                            <span class="btn-inner--text">Book Inspection</span>
                        </a>
                    </li>
                </ul>
                <div class="d-lg-none px-4 text-center">
                    <a href="https://book.housecallpro.com/book/JCC-Roofing-Company/90f4d54160e34e2daaae20d26db6ad2d"
                        class="btn btn-block btn-sm btn-primary" target="_blank">Book Inspection</a>
                </div>
            </div>
        </div>
    </nav>
</header>

<!--============Inicio==============-->
<section class="slice py-8 py-lg-8">
    <div class="container pt-5">
        <div class="row align-items-center">
            <div class="col-12 col-md-6 col-lg-6 order-md-2 mb-5 mb-md-0">
                <div class="card border-0 shadow-lg zindex-100 mb-0">
                    <figure>
                        <img alt="Image placeholder" src="{{asset('img/testimonial-min.png')}}" class="card-img">
                    </figure>
                    <div class="card-img-overlay d-flex justify-content-end">
                        <div class="text-center">
                            <a href="https://www.youtube.com/watch?v=BO9E7i_9cx4"
                                class="btn btn-white btn-icon-only rounded-circle shadow-sm hover-scale-110"
                                data-fancybox="">
                                <span class="btn-inner--icon text-sm">
                                    <i data-feather="play" class="text-primary"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <!--======shape======-->
                <div class="w-75 position-absolute bottom-n5 right-n5 rotate-180">
                    <img alt="Image placeholder" src="{{asset('img/shapes/bubble-2.svg')}}"
                        class="svg-inject fill-secondary" />
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-6 order-md-1 pr-md-5">
                <h1 class="h1 mb-4 font-weight-700 lh-120">
                    FULL ROOF REPLACEMENTS FROM $6,999
                </h1>
                <p class="lead text-muted">
                    Get a new roof and new peace of mind today! Call us for a free roof inspection and estimate.
                </p>
                <div class="mt-5">
                    <a href="https://book.housecallpro.com/book/JCC-Roofing-Company/90f4d54160e34e2daaae20d26db6ad2d"
                        target="_blank" class="btn btn-primary">
                        Book now
                    </a>
                    <a href="https://www.youtube.com/watch?v=BO9E7i_9cx4"
                        class="btn btn-neutral btn-icon d-none d-lg-inline-block" data-fancybox>
                        <span class="btn-inner--text">Watch our video</span>
                        <span class="btn-inner--icon">
                            <i data-feather="play" class="text-primary"></i>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="space"></div>
<!--============Formulario==============-->
<section class="py-3 bg-jcc-blue">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-lg-12 col-md-12">
                <h1 class="mb-4 lh-120 mx-auto text-white mt-3">
                    Get your free roof estimate
                </h1>
                <h4 class="text-white mt-2 mb-0">
                    Usually respond your request in few hours.
                </h4>
                <p class="lead lh-180 text-white">We accept insurance work and offer financing options</p>
                <div class="row mx-n2 my-3 justify-content-center">
                    <form style="display:flex;" class="justify-content-center flex-sm-row flex-column h-100"
                        id="home_form" method="POST">
                        @csrf
                        <div class="col-12 col-lg-2 px-2">
                            <div class="form-group">
                                <input id="name" class="form-control" type="text" placeholder="Name" name="name"
                                    required>
                            </div>
                        </div>
                        <div class="col-12 col-lg-2 px-2">
                            <div class="form-group">
                                <input id="phone" class="form-control" type="tel" placeholder="Phone" name="phone"
                                    required>
                            </div>
                        </div>
                        <div class="col-12 col-lg-2 px-2">
                            <div class="form-group">
                                <input class="form-control" id="email" type="email" name="email" placeholder="Email"
                                    required>
                            </div>
                        </div>
                        <div class="col-12 col-lg-3 px-2">
                            <select name="service" id="service" class="custom-select" required>
                                <option value="">Select Service</option>
                                <option value="repair">Roof Repair</option>
                                <option value="replacement">Roof Replacement</option>
                                <option value="inspection">Free Inspection</option>
                            </select>
                        </div>
                        <div class="col-auto px-2 inline-form">
                            <button id="enviar" class="btn btn-secondary px-3">
                                Submit
                            </button>
                        </div>
                    </form>
                    <div id="error_message" style="width:100%; height:100%; display:none; ">
                        <h4 style="font-weight: 800; letter-spacing: 1px;">
                            Error
                        </h4>
                        Sorry there was an error sending your form.
                    </div>
                    <div id="success_message" style="width:100%; height:100%; display:none; ">
                        <h4 style="font-weight: 800; letter-spacing: 1px;">
                            Success! Your Message was Sent Successfully.
                        </h4>
                    </div>
                </div>
                <div class="mt-2">
                    <h4 class="text-white mt-2 mb-0">
                        Request your own, free, no strings-attached honest roof estimate.
                    </h4>
                    <p class="lead lh-180 text-white">Estimates are only free if requested by the owner of the scheduled
                        property.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<!--============Houston’s Roof==============-->
<section class="slice slice-lg pt-5 pb-7 d-flex align-items-center bg-cover bg-size--cover"
    style="background-image: url({{asset('img/bg-4.jpg')}});">
    <div class="video-overlay home" style="z-index: 0;opacity: 0.7;"></div>
    <div class="container">
        <div class="row mt-5 mb-3 justify-content-center text-center">
            <div class="col-lg-6 col-md-10">
                <h6 class="text-white">A top Houston roofing company</h6>
                <h1 class="mb-4 lh-120 mx-auto text-center text-white">Houston’s Roof Replacement Experts</h1>

            </div>
        </div>
        <div class="row mx-lg-n4 justify-content-center align-items-center">
            <div class="col-lg-4 col-md-6 px-lg-4">
                <img alt="workmanship logo" class="img-fluid workmanship-logo"
                    src="{{asset('img/certifications/workmanship-logo.png')}}">
            </div>
            <div class="col-lg-5 col-md-6 px-lg-4 align-items-center">
                <h3 class="mb-4 lh-120 text-white">Properly Installed Guaranteed</h3>
                <div class="card mb-2">
                    <div class="p-2 d-flex">
                        <div>
                            <div class="icon icon-shape rounded-circle bg-primary text-white mr-4"
                                style="width:2rem;height:2rem;">
                                <i data-feather="check"></i>
                            </div>
                        </div>
                        <div>
                            <span class="h6">Per best roofing installation practices</span>
                        </div>
                    </div>
                </div>
                <div class="card mb-2">
                    <div class="p-2 d-flex">
                        <div>
                            <div class="icon icon-shape rounded-circle bg-primary text-white mr-4"
                                style="width:2rem;height:2rem;">
                                <i data-feather="check"></i>
                            </div>
                        </div>
                        <div>
                            <span class="h6">Local codes</span>

                        </div>
                    </div>
                </div>
                <div class="card mb-2">
                    <div class="p-2 d-flex">
                        <div>
                            <div class="icon icon-shape rounded-circle bg-primary text-white mr-4"
                                style="width:2rem;height:2rem;">
                                <i data-feather="check"></i>
                            </div>
                        </div>
                        <div>
                            <span class="h6">Manufacturer specifications</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--============Options==============-->
<section class="slice py-7 px-1 d-flex align-items-center bg-section-secondary">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="row row-grid center-on-mobile">
                <div class="col-md-4">
                    <div class="pb-4">
                        <div class="icon">
                            <span class="icomoon-technology roofing custom-icon-med text-jcc-blue"></span>
                        </div>
                    </div>
                    <h6 class="font-weight-800 text-uppercase">Our Technology</h6>
                    <p class="mb-0 lead">We use the latest technology to ensure the best quality service from the
                        beginning to the end of the project. We let you track and view the progress of the roof repair
                        or roof replacement work.</p>
                </div>
                <div class="col-md-4">
                    <div class="pb-4">
                        <div class="icon">
                            <span class="icomoon-hand_nailed roofing custom-icon-med text-jcc-blue"></span>
                        </div>
                    </div>
                    <h6 class="font-weight-800 text-uppercase">Hand-Nailed Roofs</h6>
                    <p class="mb-0 lead">We are proud to deliver the best-in-class roof replacement systems. Hand
                        nailing is the preferred method of installation for every roof we build. </p>
                </div>
                <div class="col-md-4">
                    <div class="pb-4">
                        <div class="icon">
                            <span class="icomoon-money_problem roofing custom-icon-med text-jcc-blue"></span>
                        </div>
                    </div>
                    <h6 class="font-weight-800 text-uppercase">Money is never a problem</h6>
                    <p class="mb-0 lead">We offer flexible financing plans to repair or replace your roof. We find the
                        solutions that work best for your needs, and your budget.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<!--============Accredited==============-->
<section class="slice slice-lg">
    <div class="container">
        <div class="row justify-content-center mb-6">
            <div class="col-lg-10 text-center">
                <h3 class="h1">Accredited & Certified Roof Contractors</h3>
                <p class="lead lh-190">
                    We are certified with the roofing manufacturer to ensure that every roof we install has the
                    warranty for your investment. We work safely to ensure your new roof is solidly in place and
                    performs the way the manufacturer designed it.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mx-auto">
                <div class="hover-blurable">
                    <a href="#">
                        <div class="blurable-item client-group row justify-content-center">
                            <div class="client col-lg-2 col-md-3 col-4 py-2">
                                <img alt="Image placeholder" src="{{asset('img/certifications/BBB.jpg')}}">
                            </div>
                            <div class="client col-lg-2 col-md-3 col-4 py-2">
                                <img alt="Image placeholder" src="{{asset('img/certifications/CertainTeed.jpg')}}">
                            </div>
                            <div class="client col-lg-2 col-md-3 col-4 py-2">
                                <img alt="Image placeholder" src="{{asset('img/certifications/Elite.jpg')}}">
                            </div>
                            <div class="client col-lg-2 col-md-3 col-4 py-2">
                                <img alt="Image placeholder" src="{{asset('img/certifications/GAF.jpg')}}">
                            </div>
                            <div class="client col-lg-2 col-md-3 col-4 py-2">
                                <img alt="Image placeholder" src="{{asset('img/certifications/Screened.jpg')}}">
                            </div>
                            <div class="client col-lg-2 col-md-3 col-4 py-2">
                                <img alt="Image placeholder" src="{{asset('img/certifications/ShingleMaster.jpg')}}">
                            </div>
                        </div>
                        <span class="blurable-hidden btn btn-sm btn-primary">See all certifications</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection