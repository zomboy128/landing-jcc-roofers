<?php

use Illuminate\Support\Facades\Route;

//Ruta Inicio
Route::get('/', 'App\Http\Controllers\LandingController@index');
//Ruta Contacto
Route::post('/contacto', 'App\Http\Controllers\LandingController@store');